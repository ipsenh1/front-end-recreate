import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello65Component } from './hello65.component';

describe('Hello65Component', () => {
  let component: Hello65Component;
  let fixture: ComponentFixture<Hello65Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello65Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello65Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
