import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello48Component } from './hello48.component';

describe('Hello48Component', () => {
  let component: Hello48Component;
  let fixture: ComponentFixture<Hello48Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello48Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello48Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
