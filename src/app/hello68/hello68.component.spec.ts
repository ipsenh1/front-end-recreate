import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello68Component } from './hello68.component';

describe('Hello68Component', () => {
  let component: Hello68Component;
  let fixture: ComponentFixture<Hello68Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello68Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello68Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
