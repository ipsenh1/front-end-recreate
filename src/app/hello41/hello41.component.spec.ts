import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello41Component } from './hello41.component';

describe('Hello41Component', () => {
  let component: Hello41Component;
  let fixture: ComponentFixture<Hello41Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello41Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello41Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
