import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello56Component } from './hello56.component';

describe('Hello56Component', () => {
  let component: Hello56Component;
  let fixture: ComponentFixture<Hello56Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello56Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello56Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
