import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello91Component } from './hello91.component';

describe('Hello91Component', () => {
  let component: Hello91Component;
  let fixture: ComponentFixture<Hello91Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello91Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello91Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
