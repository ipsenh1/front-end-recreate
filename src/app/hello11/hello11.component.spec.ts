import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello11Component } from './hello11.component';

describe('Hello11Component', () => {
  let component: Hello11Component;
  let fixture: ComponentFixture<Hello11Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello11Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello11Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
