import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello62Component } from './hello62.component';

describe('Hello62Component', () => {
  let component: Hello62Component;
  let fixture: ComponentFixture<Hello62Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello62Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello62Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
