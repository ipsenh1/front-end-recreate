import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello94Component } from './hello94.component';

describe('Hello94Component', () => {
  let component: Hello94Component;
  let fixture: ComponentFixture<Hello94Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello94Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello94Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
