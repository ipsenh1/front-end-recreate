import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello30Component } from './hello30.component';

describe('Hello30Component', () => {
  let component: Hello30Component;
  let fixture: ComponentFixture<Hello30Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello30Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello30Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
