import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello22Component } from './hello22.component';

describe('Hello22Component', () => {
  let component: Hello22Component;
  let fixture: ComponentFixture<Hello22Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello22Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello22Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
