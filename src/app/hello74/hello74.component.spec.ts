import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello74Component } from './hello74.component';

describe('Hello74Component', () => {
  let component: Hello74Component;
  let fixture: ComponentFixture<Hello74Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello74Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello74Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
