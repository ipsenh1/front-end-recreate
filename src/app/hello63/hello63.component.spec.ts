import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello63Component } from './hello63.component';

describe('Hello63Component', () => {
  let component: Hello63Component;
  let fixture: ComponentFixture<Hello63Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello63Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello63Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
