import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello82Component } from './hello82.component';

describe('Hello82Component', () => {
  let component: Hello82Component;
  let fixture: ComponentFixture<Hello82Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello82Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello82Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
