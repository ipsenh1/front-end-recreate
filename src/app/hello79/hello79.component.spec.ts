import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello79Component } from './hello79.component';

describe('Hello79Component', () => {
  let component: Hello79Component;
  let fixture: ComponentFixture<Hello79Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello79Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello79Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
