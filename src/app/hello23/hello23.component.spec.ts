import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello23Component } from './hello23.component';

describe('Hello23Component', () => {
  let component: Hello23Component;
  let fixture: ComponentFixture<Hello23Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello23Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello23Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
