import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello37Component } from './hello37.component';

describe('Hello37Component', () => {
  let component: Hello37Component;
  let fixture: ComponentFixture<Hello37Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello37Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello37Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
