import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello70Component } from './hello70.component';

describe('Hello70Component', () => {
  let component: Hello70Component;
  let fixture: ComponentFixture<Hello70Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello70Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello70Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
