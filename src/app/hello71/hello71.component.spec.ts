import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello71Component } from './hello71.component';

describe('Hello71Component', () => {
  let component: Hello71Component;
  let fixture: ComponentFixture<Hello71Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello71Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello71Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
