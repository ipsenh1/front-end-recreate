import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello67Component } from './hello67.component';

describe('Hello67Component', () => {
  let component: Hello67Component;
  let fixture: ComponentFixture<Hello67Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello67Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello67Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
