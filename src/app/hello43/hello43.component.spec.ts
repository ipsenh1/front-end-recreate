import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello43Component } from './hello43.component';

describe('Hello43Component', () => {
  let component: Hello43Component;
  let fixture: ComponentFixture<Hello43Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello43Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello43Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
