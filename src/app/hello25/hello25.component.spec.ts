import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello25Component } from './hello25.component';

describe('Hello25Component', () => {
  let component: Hello25Component;
  let fixture: ComponentFixture<Hello25Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello25Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello25Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
