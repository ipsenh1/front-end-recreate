import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello69Component } from './hello69.component';

describe('Hello69Component', () => {
  let component: Hello69Component;
  let fixture: ComponentFixture<Hello69Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello69Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello69Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
