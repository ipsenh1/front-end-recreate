import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello100Component } from './hello100.component';

describe('Hello100Component', () => {
  let component: Hello100Component;
  let fixture: ComponentFixture<Hello100Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello100Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello100Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
