import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello80Component } from './hello80.component';

describe('Hello80Component', () => {
  let component: Hello80Component;
  let fixture: ComponentFixture<Hello80Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello80Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello80Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
