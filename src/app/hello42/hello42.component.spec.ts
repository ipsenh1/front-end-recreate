import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello42Component } from './hello42.component';

describe('Hello42Component', () => {
  let component: Hello42Component;
  let fixture: ComponentFixture<Hello42Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello42Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello42Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
