import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello90Component } from './hello90.component';

describe('Hello90Component', () => {
  let component: Hello90Component;
  let fixture: ComponentFixture<Hello90Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello90Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello90Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
