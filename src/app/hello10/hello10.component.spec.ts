import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello10Component } from './hello10.component';

describe('Hello10Component', () => {
  let component: Hello10Component;
  let fixture: ComponentFixture<Hello10Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello10Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello10Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
