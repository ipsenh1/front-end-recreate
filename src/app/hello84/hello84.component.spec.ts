import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello84Component } from './hello84.component';

describe('Hello84Component', () => {
  let component: Hello84Component;
  let fixture: ComponentFixture<Hello84Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello84Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello84Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
