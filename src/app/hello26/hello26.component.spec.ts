import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello26Component } from './hello26.component';

describe('Hello26Component', () => {
  let component: Hello26Component;
  let fixture: ComponentFixture<Hello26Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello26Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello26Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
