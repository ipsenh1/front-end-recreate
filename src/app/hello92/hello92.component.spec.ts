import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello92Component } from './hello92.component';

describe('Hello92Component', () => {
  let component: Hello92Component;
  let fixture: ComponentFixture<Hello92Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello92Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello92Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
