import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello97Component } from './hello97.component';

describe('Hello97Component', () => {
  let component: Hello97Component;
  let fixture: ComponentFixture<Hello97Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello97Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello97Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
