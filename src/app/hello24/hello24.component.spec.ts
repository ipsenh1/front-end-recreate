import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello24Component } from './hello24.component';

describe('Hello24Component', () => {
  let component: Hello24Component;
  let fixture: ComponentFixture<Hello24Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello24Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello24Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
