import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import { HelloComponent } from './hello/hello.component';
import { Hello1Component } from './hello1/hello1.component';
import { Hello2Component } from './hello2/hello2.component';
import { Hello3Component } from './hello3/hello3.component';
import { Hello4Component } from './hello4/hello4.component';
import { Hello5Component } from './hello5/hello5.component';
import { Hello6Component } from './hello6/hello6.component';
import { Hello7Component } from './hello7/hello7.component';
import { Hello8Component } from './hello8/hello8.component';
import { Hello9Component } from './hello9/hello9.component';
import { Hello10Component } from './hello10/hello10.component';
import { Hello11Component } from './hello11/hello11.component';
import { Hello12Component } from './hello12/hello12.component';
import { Hello13Component } from './hello13/hello13.component';
import { Hello14Component } from './hello14/hello14.component';
import { Hello15Component } from './hello15/hello15.component';
import { Hello16Component } from './hello16/hello16.component';
import { Hello17Component } from './hello17/hello17.component';
import { Hello18Component } from './hello18/hello18.component';
import { Hello19Component } from './hello19/hello19.component';
import { Hello20Component } from './hello20/hello20.component';
import { Hello21Component } from './hello21/hello21.component';
import { Hello22Component } from './hello22/hello22.component';
import { Hello23Component } from './hello23/hello23.component';
import { Hello24Component } from './hello24/hello24.component';
import { Hello25Component } from './hello25/hello25.component';
import { Hello26Component } from './hello26/hello26.component';
import { Hello27Component } from './hello27/hello27.component';
import { Hello28Component } from './hello28/hello28.component';
import { Hello29Component } from './hello29/hello29.component';
import { Hello30Component } from './hello30/hello30.component';
import { Hello31Component } from './hello31/hello31.component';
import { Hello32Component } from './hello32/hello32.component';
import { Hello33Component } from './hello33/hello33.component';
import { Hello34Component } from './hello34/hello34.component';
import { Hello35Component } from './hello35/hello35.component';
import { Hello36Component } from './hello36/hello36.component';
import { Hello37Component } from './hello37/hello37.component';
import { Hello38Component } from './hello38/hello38.component';
import { Hello39Component } from './hello39/hello39.component';
import { Hello40Component } from './hello40/hello40.component';
import { Hello41Component } from './hello41/hello41.component';
import { Hello42Component } from './hello42/hello42.component';
import { Hello43Component } from './hello43/hello43.component';
import { Hello44Component } from './hello44/hello44.component';
import { Hello45Component } from './hello45/hello45.component';
import { Hello46Component } from './hello46/hello46.component';
import { Hello47Component } from './hello47/hello47.component';
import { Hello48Component } from './hello48/hello48.component';
import { Hello49Component } from './hello49/hello49.component';
import { Hello450Component } from './hello450/hello450.component';
import { Hello51Component } from './hello51/hello51.component';
import { Hello52Component } from './hello52/hello52.component';
import { Hello53Component } from './hello53/hello53.component';
import { Hello54Component } from './hello54/hello54.component';
import { Hello55Component } from './hello55/hello55.component';
import { Hello56Component } from './hello56/hello56.component';
import { Hello57Component } from './hello57/hello57.component';
import { Hello58Component } from './hello58/hello58.component';
import { Hello59Component } from './hello59/hello59.component';
import { Hello60Component } from './hello60/hello60.component';
import { Hello61Component } from './hello61/hello61.component';
import { Hello62Component } from './hello62/hello62.component';
import { Hello63Component } from './hello63/hello63.component';
import { Hello64Component } from './hello64/hello64.component';
import { Hello65Component } from './hello65/hello65.component';
import { Hello66Component } from './hello66/hello66.component';
import { Hello67Component } from './hello67/hello67.component';
import { Hello68Component } from './hello68/hello68.component';
import { Hello69Component } from './hello69/hello69.component';
import { Hello70Component } from './hello70/hello70.component';
import { Hello71Component } from './hello71/hello71.component';
import { Hello72Component } from './hello72/hello72.component';
import { Hello73Component } from './hello73/hello73.component';
import { Hello74Component } from './hello74/hello74.component';
import { Hello75Component } from './hello75/hello75.component';
import { Hello76Component } from './hello76/hello76.component';
import { Hello77Component } from './hello77/hello77.component';
import { Hello78Component } from './hello78/hello78.component';
import { Hello79Component } from './hello79/hello79.component';
import { Hello80Component } from './hello80/hello80.component';
import { Hello81Component } from './hello81/hello81.component';
import { Hello82Component } from './hello82/hello82.component';
import { Hello83Component } from './hello83/hello83.component';
import { Hello84Component } from './hello84/hello84.component';
import { Hello85Component } from './hello85/hello85.component';
import { Hello86Component } from './hello86/hello86.component';
import { Hello87Component } from './hello87/hello87.component';
import { Hello88Component } from './hello88/hello88.component';
import { Hello89Component } from './hello89/hello89.component';
import { Hello90Component } from './hello90/hello90.component';
import { Hello91Component } from './hello91/hello91.component';
import { Hello92Component } from './hello92/hello92.component';
import { Hello93Component } from './hello93/hello93.component';
import { Hello94Component } from './hello94/hello94.component';
import { Hello95Component } from './hello95/hello95.component';
import { Hello96Component } from './hello96/hello96.component';
import { Hello97Component } from './hello97/hello97.component';
import { Hello98Component } from './hello98/hello98.component';
import { Hello99Component } from './hello99/hello99.component';
import { Hello100Component } from './hello100/hello100.component';

@NgModule({
  declarations: [
    AppComponent,
    HelloComponent,
    Hello1Component,
    Hello2Component,
    Hello3Component,
    Hello4Component,
    Hello5Component,
    Hello6Component,
    Hello7Component,
    Hello8Component,
    Hello9Component,
    Hello10Component,
    Hello11Component,
    Hello12Component,
    Hello13Component,
    Hello14Component,
    Hello15Component,
    Hello16Component,
    Hello17Component,
    Hello18Component,
    Hello19Component,
    Hello20Component,
    Hello21Component,
    Hello22Component,
    Hello23Component,
    Hello24Component,
    Hello25Component,
    Hello26Component,
    Hello27Component,
    Hello28Component,
    Hello29Component,
    Hello30Component,
    Hello31Component,
    Hello32Component,
    Hello33Component,
    Hello34Component,
    Hello35Component,
    Hello36Component,
    Hello37Component,
    Hello38Component,
    Hello39Component,
    Hello40Component,
    Hello41Component,
    Hello42Component,
    Hello43Component,
    Hello44Component,
    Hello45Component,
    Hello46Component,
    Hello47Component,
    Hello48Component,
    Hello49Component,
    Hello450Component,
    Hello51Component,
    Hello52Component,
    Hello53Component,
    Hello54Component,
    Hello55Component,
    Hello56Component,
    Hello57Component,
    Hello58Component,
    Hello59Component,
    Hello60Component,
    Hello61Component,
    Hello62Component,
    Hello63Component,
    Hello64Component,
    Hello65Component,
    Hello66Component,
    Hello67Component,
    Hello68Component,
    Hello69Component,
    Hello70Component,
    Hello71Component,
    Hello72Component,
    Hello73Component,
    Hello74Component,
    Hello75Component,
    Hello76Component,
    Hello77Component,
    Hello78Component,
    Hello79Component,
    Hello80Component,
    Hello81Component,
    Hello82Component,
    Hello83Component,
    Hello84Component,
    Hello85Component,
    Hello86Component,
    Hello87Component,
    Hello88Component,
    Hello89Component,
    Hello90Component,
    Hello91Component,
    Hello92Component,
    Hello93Component,
    Hello94Component,
    Hello95Component,
    Hello96Component,
    Hello97Component,
    Hello98Component,
    Hello99Component,
    Hello100Component
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
