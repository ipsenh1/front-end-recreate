import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello16Component } from './hello16.component';

describe('Hello16Component', () => {
  let component: Hello16Component;
  let fixture: ComponentFixture<Hello16Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello16Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello16Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
