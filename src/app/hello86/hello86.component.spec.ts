import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello86Component } from './hello86.component';

describe('Hello86Component', () => {
  let component: Hello86Component;
  let fixture: ComponentFixture<Hello86Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello86Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello86Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
