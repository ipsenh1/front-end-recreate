import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello75Component } from './hello75.component';

describe('Hello75Component', () => {
  let component: Hello75Component;
  let fixture: ComponentFixture<Hello75Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello75Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello75Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
