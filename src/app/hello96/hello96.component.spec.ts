import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello96Component } from './hello96.component';

describe('Hello96Component', () => {
  let component: Hello96Component;
  let fixture: ComponentFixture<Hello96Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello96Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello96Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
