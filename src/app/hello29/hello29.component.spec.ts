import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello29Component } from './hello29.component';

describe('Hello29Component', () => {
  let component: Hello29Component;
  let fixture: ComponentFixture<Hello29Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello29Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello29Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
