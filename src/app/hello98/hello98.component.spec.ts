import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello98Component } from './hello98.component';

describe('Hello98Component', () => {
  let component: Hello98Component;
  let fixture: ComponentFixture<Hello98Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello98Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello98Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
