import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello88Component } from './hello88.component';

describe('Hello88Component', () => {
  let component: Hello88Component;
  let fixture: ComponentFixture<Hello88Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello88Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello88Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
