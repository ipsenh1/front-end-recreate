import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello46Component } from './hello46.component';

describe('Hello46Component', () => {
  let component: Hello46Component;
  let fixture: ComponentFixture<Hello46Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello46Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello46Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
