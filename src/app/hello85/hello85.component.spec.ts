import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello85Component } from './hello85.component';

describe('Hello85Component', () => {
  let component: Hello85Component;
  let fixture: ComponentFixture<Hello85Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello85Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello85Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
