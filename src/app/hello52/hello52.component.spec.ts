import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello52Component } from './hello52.component';

describe('Hello52Component', () => {
  let component: Hello52Component;
  let fixture: ComponentFixture<Hello52Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello52Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello52Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
