import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Hello87Component } from './hello87.component';

describe('Hello87Component', () => {
  let component: Hello87Component;
  let fixture: ComponentFixture<Hello87Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Hello87Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Hello87Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
